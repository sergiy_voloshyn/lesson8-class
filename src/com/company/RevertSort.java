package com.company;

import java.util.Comparator;

/**
 * Created by Student on 12.12.2017.
 */
public class RevertSort implements Comparator<String> {


    @Override
    public int compare(String o1, String o2) {
        return -o2.compareTo(o2);
    }
}
